package com.example.sample13;

import junit.framework.TestCase;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


public class MainActivityTest{
        MainActivity ma = new MainActivity();
        String x1="1";
        String y1="2";
        int checked = R.id.radioButtonAddition;
        @Test
        public void testMakeResult() {
            assertThat(ma.makeResult(x1,y1,checked),is("1 + 2 = 3.0"));
        }
}
